(function ($) {

  Drupal.behaviors.address_template = {
    attach:function (context, settings) {
      $('a.addr-temp-ch', context).once('addr-temp-active').click(function(e) {
        e.preventDefault();
        var customer_profile = $(this).parents('.customer_profile_billing');
        var old_address = $('select.form-select', customer_profile).val();
        $("input.old-addressbook-id", customer_profile).val(old_address);

        $('select.form-select', customer_profile).val('none').change();
      });

      $('select[name="customer_profile_billing[addressbook]"]', context).once('addr-temp-default', function(e) {
        var customer_profile = $(this).parents('form');
        var old_address = $('select.form-select', customer_profile).val();
        if ('none' != old_address) {
          $("input.old-addressbook-id", customer_profile).val(old_address);
        }
      });
    }

  };
})(jQuery);
