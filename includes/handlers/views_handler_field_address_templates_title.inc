<?php
/**
 * @file
 * Handlers for present a label to a address templates rule.
 */

/**
 * Field handler to present a label to a address templates rule.
 */
class views_handler_field_address_templates_title extends views_handler_field_address_templates {
  function construct() {
    parent::construct();

    $this->additional_fields['title'] = array('table' => 'address_templates', 'field' => 'title');
    $this->additional_fields['at_id'] = array('table' => 'address_templates', 'field' => 'at_id');
    $this->additional_fields['uid'] = array('table' => 'address_templates', 'field' => 'uid');
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_address_templates_edit'] = array('default' => isset($this->definition['link_to_address_templates_edit default']) ? $this->definition['link_to_address_templates_edit default'] : FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Provide link to address templates option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_to_address_templates_edit'] = array(
      '#title' => t('Link this field to the edit page of address templates'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_address_templates_edit']),
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render whatever the data is as a link to the address templates.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    $at_id = $this->get_value($values, 'at_id');
    $uid = $this->get_value($values, 'uid');

    if (!empty($this->options['link_to_address_templates_edit']) && !empty($this->additional_fields['at_id'])) {
      if ($data !== NULL && $data !== '') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = 'user/' . $uid . '/addresses/' . $at_id . '/edit';
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    else {
      parent::render_link($data, $values);
    }

    return $data;
  }

  function render($values) {
    $text = $this->get_value($values, 'title');
    return $this->render_link($this->sanitize_value($text), $values);
  }
}
