<?php
/**
 * @file
 * Contains the basic 'address_templates' field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a
 * address_templates.
 * Definition terms:
 * - link_to_address_templates default: Should this field have the checkbox
 *   "link to address templates" enabled by default.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_address_templates extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // Don't add the additional fields to groupby.
    $this->additional_fields['title'] = array('table' => 'address_templates', 'field' => 'title');
    if (!empty($this->options['link_to_address_templates'])) {
      $this->additional_fields['at_id'] = array('table' => 'address_templates', 'field' => 'at_id');
      $this->additional_fields['uid'] = array('table' => 'address_templates', 'field' => 'uid');
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_address_templates'] = array('default' => isset($this->definition['link_to_address_templates default']) ? $this->definition['link_to_address_templates default'] : FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Provide link to address templates option.
   */
  function options_form(&$form, &$form_state) {
    $form['link_to_address_templates'] = array(
      '#title' => t('Link this field to the original piece of address templates'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_address_templates']),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the address templates.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    $at_id = $this->get_value($values, 'at_id');
    $uid = $this->get_value($values, 'uid');

    if (!empty($this->options['link_to_address_templates']) && !empty($this->additional_fields['at_id'])) {
      if ($data !== NULL && $data !== '') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = 'user/' . $uid . '/addresses/' . $at_id;
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }
}
