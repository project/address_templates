<?php
/**
 * @file
 * Handlers for present a link to a address templates rule.
 */

/**
 * Field handler to present a link to a address templates rule.
 */
class views_handler_field_address_templates_link extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['at_id'] = array('table' => 'address_templates', 'field' => 'at_id');
    $this->additional_fields['uid'] = array('table' => 'address_templates', 'field' => 'uid');
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $at_id = $this->get_value($values, 'at_id');
    $uid = $this->get_value($values, 'uid');
    $text = !empty($this->options['text']) ? $this->options['text'] : t('View');

    return l($text, 'user/' . $uid . '/addresses/' . $at_id);
  }
}
