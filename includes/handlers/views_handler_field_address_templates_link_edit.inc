<?php
/**
 * @file
 * Handlers for present a link to a address templates edit link.
 */

/**
 * Field handler to present a address templates edit link.
 */
class views_handler_field_address_templates_link_edit extends views_handler_field_address_templates_link {
  function render($values) {
    $at_id = $this->get_value($values, 'at_id');
    $uid = $this->get_value($values, 'uid');

    $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');

    return l($text, 'user/' . $uid . '/addresses/' . $at_id . '/edit');
  }
}
