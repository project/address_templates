<?php
/**
 * @file
 * address_templates.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function address_templates_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'address_templates_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'address_templates';
  $view->human_name = 'Addresses';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Addresses';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '8';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['columns'] = '2';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'edit' => 'edit',
    'delete' => 'delete',
  );
  /* Field: Address templates: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'address_templates';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_address_templates_edit'] = TRUE;
  /* Field: Address templates: Address */
  $handler->display->display_options['fields']['at_address']['id'] = 'at_address';
  $handler->display->display_options['fields']['at_address']['table'] = 'field_data_at_address';
  $handler->display->display_options['fields']['at_address']['field'] = 'at_address';
  $handler->display->display_options['fields']['at_address']['label'] = '';
  $handler->display->display_options['fields']['at_address']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['at_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['at_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  /* Field: Address templates: Defaults */
  $handler->display->display_options['fields']['at_defaults']['id'] = 'at_defaults';
  $handler->display->display_options['fields']['at_defaults']['table'] = 'field_data_at_defaults';
  $handler->display->display_options['fields']['at_defaults']['field'] = 'at_defaults';
  $handler->display->display_options['fields']['at_defaults']['label'] = '';
  $handler->display->display_options['fields']['at_defaults']['element_type'] = 'strong';
  $handler->display->display_options['fields']['at_defaults']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['at_defaults']['delta_offset'] = '0';
  /* Field: Address templates: Edit link */
  $handler->display->display_options['fields']['edit']['id'] = 'edit';
  $handler->display->display_options['fields']['edit']['table'] = 'address_templates';
  $handler->display->display_options['fields']['edit']['field'] = 'edit';
  $handler->display->display_options['fields']['edit']['label'] = '';
  $handler->display->display_options['fields']['edit']['element_label_colon'] = FALSE;
  /* Field: Address templates: Delete link */
  $handler->display->display_options['fields']['delete']['id'] = 'delete';
  $handler->display->display_options['fields']['delete']['table'] = 'address_templates';
  $handler->display->display_options['fields']['delete']['field'] = 'delete';
  $handler->display->display_options['fields']['delete']['label'] = '';
  $handler->display->display_options['fields']['delete']['element_label_colon'] = FALSE;
  /* Sort criterion: Address templates: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'address_templates';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: Address templates: User id */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'address_templates';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['uid']['validate']['type'] = 'user';
  $handler->display->display_options['arguments']['uid']['validate_options']['type'] = 'either';
  /* Filter criterion: Address templates: Defaults (at_defaults) */
  $handler->display->display_options['filters']['at_defaults_value']['id'] = 'at_defaults_value';
  $handler->display->display_options['filters']['at_defaults_value']['table'] = 'field_data_at_defaults';
  $handler->display->display_options['filters']['at_defaults_value']['field'] = 'at_defaults_value';
  $handler->display->display_options['filters']['at_defaults_value']['operator'] = 'empty';

  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '2';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Address templates: Defaults (at_defaults) */
  $handler->display->display_options['filters']['at_defaults_value']['id'] = 'at_defaults_value';
  $handler->display->display_options['filters']['at_defaults_value']['table'] = 'field_data_at_defaults';
  $handler->display->display_options['filters']['at_defaults_value']['field'] = 'at_defaults_value';
  $handler->display->display_options['filters']['at_defaults_value']['operator'] = 'not empty';
  $handler->display->display_options['filters']['at_defaults_value']['group'] = 1;
  $handler->display->display_options['displays'] = array(
    'default' => 'default',
    'page' => 'page',
  );
  $translatables['address_templates_list'] = array(
    t('Master'),
    t('Addresses'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('All'),
    t('Attachment'),
  );
  $export['address_templates_list'] = $view;

  return $export;
}
