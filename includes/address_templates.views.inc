<?php
/**
 * @file
 * Views definition file.
 */

/**
 * Implements hook_views_data().
 */
function address_templates_views_data() {
  $data = array();

  // Address templates.
  $data['address_templates']['table']['group']  = t('Address templates');
  $data['address_templates']['table']['entity type'] = 'address_templates';

  $data['address_templates']['table']['base'] = array(
    'field' => 'at_id',
    'title' => t('Address templates'),
  );

  $data['address_templates']['at_id'] = array(
    'title' => t('Address templates ID'),
    'help' => t('Address templates ID.'),
    'field' => array(
      'handler' => 'views_handler_field_address_templates',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['address_templates']['title'] = array(
    'title' => t('Title'),
    'field' => array(
      'title' => t('Title'),
      'handler' => 'views_handler_field_address_templates_title',
      'help' => t('The title of the address templates.'),
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['address_templates']['uid'] = array(
    'title' => t('User id'),
    'help' => t('The user created the address templates. If you need more fields than the uid add the address templates: user relationship'),
    'relationship' => array(
      'title' => t('User'),
      'help' => t('Relate address templates to the user who created it.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('user'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
  );

  $data['address_templates']['status'] = array(
    'title' => t('Status'),
    'help' => t('Boolean indicating whether the address templates is active. Usage depends on how the using module makes use of it.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'enabled-disabled' => array(t('Enabled'), t('Disabled')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Status'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['address_templates']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the address templates was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['address_templates']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the address templates was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['address_templates']['view'] = array(
    'field' => array(
      'title' => t('View link'),
      'help' => t('Provide a simple link to view address templates.'),
      'handler' => 'views_handler_field_address_templates_link',
    ),
  );

  $data['address_templates']['edit'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit address templates.'),
      'handler' => 'views_handler_field_address_templates_link_edit',
    ),
  );

  $data['address_templates']['delete'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete address templates.'),
      'handler' => 'views_handler_field_address_templates_link_delete',
    ),
  );

  return $data;
}
