<?php
/**
 * @file
 * The address templates type frontend functions.
 */

/**
 * Menu callback: address templates types page.
 */
function address_templates_ui_types($form, $form_state) {
  $form['address_templates_limit'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Limit'),
    '#description' => t(''),
    '#default_value' => variable_get('address_templates_limit', 0),
  );

  $form['address_templates_title_token'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title token'),
    '#default_value' => variable_get('address_templates_title_token', '[address-templates:at_address:postal-code] [address-templates:at_address:locality] [address-templates:at_address:thoroughfare]'),
  );

  $form['address_templates_tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Usable tokens'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['address_templates_tokens']['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(
      'site',
      'user',
      'address-templates',
    ),
    '#global_types' => FALSE,
    '#click_insert' => FALSE,
    '#show_restricted' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );

  return system_settings_form($form);
}
