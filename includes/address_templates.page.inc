<?php
/**
 * @file
 * The address templates frontend functions.
 */

/**
 * Menu callback: list the address templates.
 */
function address_templates_ui_list($user) {
  $output = views_embed_view(variable_get('address_templates_list_view_name', 'address_templates_list'), variable_get('address_templates_list_view_display', 'default'), $user->uid);

  if (empty($output)) {
    $output = t('Not found address.');
  }

  return $output;
}

/**
 * Menu callback: view a address templates.
 */
function address_templates_view_page($address_templates) {
  return address_templates_view($address_templates);
}

/**
 * Menu callback: create a address templates.
 */
function address_templates_ui_add($user) {
  $address_templates = address_templates_new();
  $address_templates->status = 1;
  $address_templates->uid = $user->uid;

  return drupal_get_form('address_templates_ui_addedit_form', $address_templates);
}

/**
 * Menu callback: edit a address templates.
 */
function address_templates_ui_edit($address_templates) {
  drupal_set_title(t('Edit @label', array('@label' => $address_templates->title)));

  return drupal_get_form('address_templates_ui_addedit_form', $address_templates);
}

/**
 * Form callback: create or edit a address templates.
 */
function address_templates_ui_addedit_form($form, &$form_state, $address_templates) {
  // Add the field related form elements.
  $form_state['address_templates'] = $address_templates;

  field_attach_form('address_templates', $address_templates, $form, $form_state);

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('Title'),
    '#default_value' => !empty($address_templates->title) ? $address_templates->title : NULL,
  );

  if (isset($form['title']) && !isset($form['title']['#weight'])) {
    $form['title']['#weight'] = -5;
  }

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#description' => t('If disabled, it does not appear in the list.'),
    '#options' => array(
      '1' => t('Active'),
      '0' => t('Disabled'),
    ),
    '#default_value' => $address_templates->status,
    '#required' => TRUE,
  );

  if (isset($form['status']) && !isset($form['status']['#weight'])) {
    $form['status']['#weight'] = 35;
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array_merge($submit, array('address_templates_ui_addedit_form_submit')),
  );

  if (!empty($form_state['address_templates']->at_id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array_merge($submit, array('address_templates_ui_delete_button_form_submit')),
      '#weight' => 45,
    );
  }

  $lang = $form['at_defaults']['#language'];
  $form['at_defaults'][$lang]['#title'] = '';

  return $form;
}

/**
 * Validate callback for address_templates_ui_addedit_form().
 */
function address_templates_ui_addedit_form_validate($form, &$form_state) {
  $address_templates = &$form_state['address_templates'];
  if (empty($address_templates->at_id)) {
    $limit = variable_get('address_templates_limit', 0);
    if ($limit > 0) {
      $created_address_template = address_templates_count($address_templates->uid);

      if ($created_address_template >= $limit) {
        $limit_error = format_plural($limit, 'You can only create 1 address.', 'You can only create @count addresses.');
        form_set_error('', $limit_error);
      }
    }
  }
}

/**
 * Submit callback for address_templates_ui_addedit_form().
 */
function address_templates_ui_addedit_form_submit($form, &$form_state) {
  global $user;

  $address_templates = &$form_state['address_templates'];

  // Save default parameters back into the $address_templates object.
  $address_templates->title = $form_state['values']['title'];
  $address_templates->status = $form_state['values']['status'];

  // Set the address templates's uid if it's being created at this time.
  if (empty($address_templates->at_id) && empty($address_templates->uid)) {
    $address_templates->uid = $user->uid;
  }

  // Notify field widgets.
  field_attach_submit('address_templates', $address_templates, $form, $form_state);

  $address_templates->revision = FALSE;
  $address_templates->log = '';

  // Save the address templates.
  address_templates_save($address_templates);

  // Redirect based on the button clicked.
  drupal_set_message(t('Address saved.'));
  $form_state['redirect'] = 'user/' . $address_templates->uid . '/addresses';
}

/**
 * Submit callback for delete button on address_templates_ui_addedit_form().
 */
function address_templates_ui_delete_button_form_submit($form, &$form_state) {
  $address_templates = &$form_state['address_templates'];

  $form_state['redirect'] = 'user/' . $address_templates->uid . '/addresses/' . $address_templates->at_id . '/delete';
}

/**
 * Form callback: confirmation form for deleting a address templates.
 *
 * @param $address_templates
 *   The address templates object to deleted.
 */
function address_templates_ui_delete_form($form, &$form_state, $address_templates) {
  $form_state['address_templates'] = $address_templates;

  $form = confirm_form($form,
    t('Are you sure you want to delete the %name?', array('%name' => $address_templates->title)),
    'user/' . $address_templates->uid . '/addresses',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for address_templates_ui_delete_form().
 */
function address_templates_ui_delete_form_submit($form, &$form_state) {
  $address_templates = $form_state['address_templates'];
  address_templates_delete($address_templates->at_id);

  drupal_set_message(t('%label has been deleted.', array('%label' => $address_templates->title)));
  watchdog('address_templates.page', 'Deleted address templates %label.', array('%label' => $address_templates->title), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'user/' . $address_templates->uid . '/addresses';
}
